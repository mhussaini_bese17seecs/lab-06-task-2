#include <iostream>
#include <vector>
#include <assert.h>
#include <time.h>
#include <algorithm>

using namespace std;

bool moveMin(vector<int> &in, vector<int> &out) 
{
	out = in;
	int tmp;
	for (int i = 0; i < out.size(); i++)
	{
		for (int j = i + 1; j < out.size(); j++)
		{
			if (out[i] > out[j])
			{
				tmp = out[j];
				out[j] = out[i];
				out[i] = tmp;
			}
		}
	}
	return true;
}

void testMoveMin()
{
	srand(time(NULL));
	int random;

	vector<int> test;

	for (int i = 0; i < 20; i++)
	{
		random = (rand() % 100) + 1;
		test.push_back(random);
	}

	sort(test.begin(), test.end());

	random = (rand() % 100) + 1;
	test.push_back(random);

	vector<int> comparer = test;
	sort(comparer.begin(), comparer.end());

	vector<int> output;
	moveMin(test, output);
	assert(comparer == output);

}


int main()
{
	vector<int> input = { 3,5,12,24,25,27,15 };
	vector<int> output;

	moveMin(input, output);

	testMoveMin();
	for (int i = 0; i < output.size(); i++)
	{
		cout << output[i] << " ";
	}

	
	cin.get();
}


